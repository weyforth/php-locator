<?php
/**
 * Json Response.
 *
 * Locates the current machine by scanning local wifi hotspots
 *
 * @author    Mike Farrow <contact@mikefarrow.co.uk>
 * @license   Proprietary/Closed Source
 * @copyright Mike Farrow
 */

namespace Weyforth\Locator;

use Guzzle\Http\Client;
use Guzzle\Http\QueryAggregator\DuplicateAggregator;

class Locator{

	static protected $browserLocation = 'https://maps.googleapis.com/maps/api/browserlocation/json';

	public static function locate(){
		$client = new Client();
		$request = $client->createRequest('GET', self::$browserLocation);
		$query = $request->getQuery();

		$query->setAggregator(new DuplicateAggregator);

		$query->set('browser', 'firefox');
		$query->set('sensor', true);

		$scan = shell_exec( __DIR__ . '/../../../bin/scan');

		if($scan != "0"){
			$scan_results = explode("\n", $scan);
			$scan_results = array_filter($scan_results);

			if(count($scan_results) != 0){
				foreach($scan_results as $scan_result){
					$info = explode("|", $scan_result);

					if(count($info) == 3){
						$query->add('wifi', 'mac:' . $info[0] . '|ssid:' . $info[1] . '|ss:' . $info[2]);
					}
				}
			}
		}

		$response = $client->send($request);

		print_r($response->json());
	}

}
